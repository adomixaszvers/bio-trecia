import os
import shlex
import subprocess
import sys

from Bio import Entrez, SeqIO
from Bio.Align.Applications import MafftCommandline

blogiukai = [
    16,
    18,
    31,
    33,
    35,
]
neutralus = [
    6,
    11,
    40,
    42,
    43,
    44,
]


def main():
    Entrez.email = "adomixaszvers@gmail.com"
    gis = dict()
    dirname = '1dalis'
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
    term = '"Human papillomavirus type %d"[Organism] AND "L1 gene"[title] AND "complete"[title]'
    with open('1dalis/blogiukai.fasta', 'w') as fasta:
        for tipas in blogiukai:
            handle = Entrez.esearch(db="nucleotide", term=term % tipas, retmax=1000)
            record = Entrez.read(handle)
            gis[tipas] = record['IdList']
            if len(gis[tipas]) == 0:
                continue
            handle = Entrez.efetch(db="nucleotide", id=','.join(gis[tipas]), rettype='fasta', retmode='text')
            fasta.write(handle.read())
    with open('1dalis/neutralus.fasta', 'w') as fasta:
        for tipas in neutralus:
            handle = Entrez.esearch(db="nucleotide", term=term % tipas, retmax=1000)
            record = Entrez.read(handle)
            gis[tipas] = record['IdList']
            if len(gis[tipas]) == 0:
                continue
            handle = Entrez.efetch(db="nucleotide", id=','.join(gis[tipas]), rettype='fasta', retmode='text')
            fasta.write(handle.read())
    dirname = '2dalis'
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
    cmdline = "cd-hit -i 1dalis/blogiukai.fasta -o 2dalis/blogiukai.fasta -T 0 -s 0.9"
    args = shlex.split(cmdline)
    with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
        sys.stdout.buffer.write(proc.stdout.read())
    cmdline = "cd-hit -i 1dalis/neutralus.fasta -o 2dalis/neutralus.fasta -T 0 -s 0.9"
    args = shlex.split(cmdline)
    with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
        sys.stdout.buffer.write(proc.stdout.read())
    sequences = []
    for record in SeqIO.parse("2dalis/blogiukai.fasta", "fasta"):
        sequences.append(record)
    for record in SeqIO.parse("2dalis/neutralus.fasta", "fasta"):
        sequences.append(record)
    SeqIO.write(sequences, "visi.fasta", "fasta")
    mafft_cline = MafftCommandline(input="visi.fasta")
    stdout, stderr = mafft_cline()
    with open("visisulygiuoti.fasta", 'w') as handle:
        handle.write(stdout)


if __name__ == "__main__":
    main()
