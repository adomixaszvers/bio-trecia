from Bio import SeqIO
from Bio import motifs
import pprint
import sys
import subprocess, shlex


def meme_wrapper(minw, maxw):
    cmdline = 'meme-psp-gen -pos 2dalis/blogiukai.fasta -neg 2dalis/neutralus.fasta -minw %d -maxw %d' % (minw, maxw)
    args = shlex.split(cmdline)
    with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
        with open('psp.gen', 'wb') as handle:
            handle.write(proc.stdout.read())
    cmdline = 'meme-meme 2dalis/blogiukai.fasta -psp psp.gen -minw %d -mod oops -maxw %d' % (minw, maxw)
    args = shlex.split(cmdline)
    with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
        sys.stdout.buffer.write(proc.stdout.read())


def hamming_distance(s1, s2):
    """Return the Hamming distance between equal-length sequences"""
    if len(s1) != len(s2):
        raise ValueError("Undefined for sequences of unequal length")
    return sum(bool(ord(ch1) - ord(ch2)) for ch1, ch2 in zip(s1, s2))


def k_mismatch(seq, subseq, min_mismatch=0, max_mismatch=None):
    m = len(seq)
    n = len(subseq)
    if max_mismatch is None:
        max_mismatch = n
    for i in range(m - n + 1):
        string = seq[i:i + n]
        distance = hamming_distance(string, subseq)
        if min_mismatch <= distance <= max_mismatch:
            yield i, distance, string


def main(filename, check=False, min_mismatch=0, max_mismatch=None):
    records = [record for record in SeqIO.parse(filename, 'fasta')]
    matches = dict()
    used = set()
    with open('meme_out/meme.txt', 'r') as handle:
        m = motifs.read(handle, 'meme')
        for instance in m.instances:
            for i in range(len(records)):
                if len(list(k_mismatch(str(records[i].seq), str(instance), max_mismatch=max_mismatch))) > 0:
                    if matches.get(i, None) is None:
                        matches[i] = []
                    matches[i].append(instance)
                    used.add(str(instance))
        pp = pprint.PrettyPrinter(indent=4)
        print('Teigiami %d iš %d'% (len(matches), len(records)))
        if check:
            print('Neigiami:')
            for i in range(len(records)):
                if i not in matches.keys():
                    print(records[i].description)
        print('Teigiamos probės:')
        pp.pprint(used)


if __name__ == "__main__":
    meme_wrapper(25, 35)
    print('Failas: 1dalis/blogiukai.fasta')
    main('1dalis/blogiukai.fasta', check=True, max_mismatch=2)
    print('1dalis/neutralus.fasta')
    main('1dalis/neutralus.fasta', max_mismatch=3)
